import { MomentJsPage } from './app.po';

describe('Angular4-useJS App', () => {
  let page: MomentJsPage;

  beforeEach(() => {
    page = new MomentJsPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
